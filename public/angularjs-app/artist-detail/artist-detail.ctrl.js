angular.module("app").controller("artistDetailController", ArtistDetailController);

function ArtistDetailController(ArtistFactory, $routeParams) {
    const vm = this;
    const artistId = $routeParams.artistId;
    ArtistFactory.getArtist(artistId).then(function (result) {
        vm.artist = result;
    });

}