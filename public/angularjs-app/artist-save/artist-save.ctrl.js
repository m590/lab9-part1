angular.module("app").controller("artistAddController", ArtistAddController);

function ArtistAddController(ArtistFactory, $location) {
    const vm = this;
    vm.newArtist = {};
    vm.artistId = $location.$$search.id;
    if (vm.artistId) {
        vm.title = 'Update an artist';
        ArtistFactory.getArtist(vm.artistId).then(function (result) {
            vm.newArtist = result;
            vm.newArtist.hits = vm.newArtist.hits.join(',');
        });
    } else {
        vm.title = 'Add an artist';
    }

    vm.saveArtist = function () {
        if (vm.newArtistForm.$valid) {
            vm.newArtist.hits = vm.newArtist.hits.split(',');
            if (vm.artistId) {
                ArtistFactory.updateArtist(vm.newArtist).then(function (result) {
                    $location.url('/');
                });
            } else {
                ArtistFactory.postArtist(vm.newArtist).then(function (result) {
                    $location.url('/');
                });
            }
        }
    }
}