const express = require('express');
const artistCtrl = require('../controller/artist.ctrl');
const albumCtrl = require('../controller/album.ctrl');

const router = express.Router();

router.route("/artists")
    .get(artistCtrl.getAllArtists)
    .post(artistCtrl.addArtist);

router.route("/artists/:artistId")
    .get(artistCtrl.getOneArtist)
    .put(artistCtrl.fullUpdateArtist)
    .patch(artistCtrl.partialUpdateArtist)
    .delete(artistCtrl.deleteArtist);

router.route("/artists/:artistId/albums")
    .get(albumCtrl.getAllAlbums)
    .post(albumCtrl.addAlbum);

router.route("/artists/:artistId/albums/:albumId")
    .get(albumCtrl.getOneAlbum)
    .put(albumCtrl.updateAlbum)
    .patch(albumCtrl.updatePartialAlbum)
    .delete(albumCtrl.deleteAlbum);

module.exports = router;