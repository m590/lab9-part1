const mongoose = require('mongoose');

const albumModel = mongoose.Schema({
    title: String,
    releasedYear: Number,
    genre: String,
    producer: String
});

const artistModel = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    bornYear: Number,
    country: String,
    albums: [albumModel],
    hits: [String]
});

mongoose.model("Artist", artistModel, "artists");