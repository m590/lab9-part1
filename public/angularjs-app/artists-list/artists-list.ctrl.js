angular.module("app").controller("artistController", ArtistController);

function ArtistController(ArtistFactory, $location) {
    const vm = this;

    const offset = 0;
    const count = 10;

    vm.loadArtists = function () {
        ArtistFactory.getAllArtists(offset, count).then(function (result) {
            vm.artists = result;
        });
    }

    vm.toAdd = function () {
        $location.url("/saveartist");
    }

    vm.toUpdate = function (artistId) {
        $location.url("/saveartist?id=" + artistId);
    }

    vm.delete = function (artistId) {
        ArtistFactory.deleteArtist(artistId).then(function (result) {
            vm.loadArtists();
        });
    }
}